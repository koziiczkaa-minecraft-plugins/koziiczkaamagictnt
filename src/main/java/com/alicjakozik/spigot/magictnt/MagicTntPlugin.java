package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.arguments.api.entity.FallbackConstants;
import org.bstats.bukkit.Metrics;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public final class MagicTntPlugin extends JavaPlugin {
	private final List<MagicTnt> magicTnts = new ArrayList<>();
	private static final String TNT_STRING = "tnt";

	@Override
	public void onEnable() {
		saveDefaultConfig();
		magicTnts.addAll(createTnts());
		setupCommand();
		getServer().getPluginManager().registerEvents(new MagicTntCraftingListener(this), this);
		getServer().getPluginManager().registerEvents(new MagicTntPlayerListener(this), this);
		new Metrics(this, 17792);
	}

	/**
	 * Creates the magic tnts.
	 *
	 * @return The list of the magic tnts.
	 */
	@NotNull
	public List<MagicTnt> createTnts() {
		final ConfigurationSection itemsConfig = getConfig().getConfigurationSection("items");
		if (itemsConfig == null) {
			throw new IllegalArgumentException("Items configuration section doesn't exist");
		}
		return itemsConfig.getKeys(false)
				.stream()
				.map(itemsConfig::getConfigurationSection)
				.filter(Objects::nonNull)
				.map(this::createMagicTnt)
				.toList();
	}

	/**
	 * Gets the config message of a specific key.
	 *
	 * @param configProperty The config message key.
	 * @return The message found in the config.
	 * @throws IllegalArgumentException When the key has not been found in the config messages section.
	 */
	@NotNull
	public String getConfigMessage(@NotNull final String configProperty) {
		final ConfigurationSection messages = getConfig().getConfigurationSection("messages");
		if (messages == null) {
			throw new IllegalArgumentException("Messages configuration section doesn't exist");
		}
		return ChatColor.translateAlternateColorCodes('&', messages.getString(configProperty, ""));
	}

	/**
	 * Based on the given item stack, filters through the list of the magic tnts.
	 *
	 * @param itemStack The item stack to filter with.
	 * @return The magic tnt if found, {@code null} otherwise.
	 */
	@Nullable
	public MagicTnt getMagicTnt(@Nullable final ItemStack itemStack) {
		if (itemStack == null) {
			return null;
		}
		return getMagicTnts().stream()
				.filter(magicTnt -> isSame(itemStack, magicTnt.itemStack()))
				.findAny()
				.orElse(null);
	}

	/**
	 * Gets the magic tnts.
	 *
	 * @return The magic tnts list.
	 */
	@NotNull
	public List<MagicTnt> getMagicTnts() {
		return magicTnts;
	}

	@NotNull
	private MagicTnt createMagicTnt(@NotNull final ConfigurationSection configurationSection) {
		final ItemStack itemStack = createMagicTntItemStack(configurationSection);
		final ConfigurationSection craftingSection =
				configurationSection.getConfigurationSection("crafting");
		if (craftingSection == null) {
			throw new UnsupportedOperationException("Crafting section is null");
		}
		final ShapedRecipe shapedRecipe = createMagicTntRecipe(craftingSection, itemStack);
		getServer().addRecipe(shapedRecipe);
		final String name = configurationSection.getName();
		return new MagicTnt(
				name,
				configurationSection.getInt("ticks", 48),
				itemStack,
				shapedRecipe,
				craftingSection.getInt("amount", 64)
		);
	}

	@NotNull
	private ItemStack createMagicTntItemStack(@NotNull final ConfigurationSection configurationSection) {
		final ItemStack magicTnt = new ItemStack(Material.TNT);
		final ItemMeta itemMeta = magicTnt.getItemMeta();
		final String name = configurationSection.getName();
		Optional.ofNullable(itemMeta).ifPresent(meta -> {
			final List<String> lore = configurationSection.getStringList("lore")
					.stream()
					.map(string -> ChatColor.translateAlternateColorCodes('&', string))
					.toList();
			meta.setLore(lore);
			final String displayName = configurationSection.getString("display-name", "MagicTNT");
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
			meta.addEnchant(Enchantment.DURABILITY, 1, false);
			meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			final PersistentDataContainer persistentDataContainer = meta.getPersistentDataContainer();
			persistentDataContainer.set(
					new NamespacedKey(this, TNT_STRING),
					PersistentDataType.STRING,
					name
			);
		});
		magicTnt.setItemMeta(itemMeta);
		return magicTnt;
	}

	@NotNull
	private ShapedRecipe createMagicTntRecipe(@NotNull final ConfigurationSection craftingSection,
											  @NotNull final ItemStack itemStack) {
		return Optional.of(craftingSection)
				.map(section -> {
					final List<String> shape = craftingSection.getStringList("shape");
					final ShapedRecipe recipe = new ShapedRecipe(createKey(itemStack), itemStack);
					recipe.shape(shape.toArray(new String[0]));
					final ConfigurationSection mapping =
							craftingSection.getConfigurationSection("mapping");
					if (mapping == null) {
						return null;
					}
					Optional.of(mapping)
							.map(keys -> keys.getKeys(false))
							.orElse(Collections.emptySet())
							.forEach(key -> Optional.of(key)
									.map(mapping::getString)
									.map(Material::matchMaterial)
									.ifPresent(material -> recipe.setIngredient(key.charAt(0), material)));
					return recipe;
				})
				.orElseThrow(UnsupportedOperationException::new);
	}

	private boolean isSame(@NotNull final ItemStack itemStack, @NotNull final ItemStack magicTnt) {
		final String itemValue = getPersistentTntValue(itemStack.getItemMeta());
		final String tntValue = getPersistentTntValue(magicTnt.getItemMeta());
		return Objects.equals(itemValue, tntValue);
	}

	@Nullable
	private String getPersistentTntValue(@Nullable final ItemMeta itemMeta) {
		if (itemMeta == null) {
			return null;
		}
		final PersistentDataContainer persistentDataContainer = itemMeta.getPersistentDataContainer();
		return persistentDataContainer.get(new NamespacedKey(this, TNT_STRING), PersistentDataType.STRING);
	}

	@NotNull
	private NamespacedKey createKey(@NotNull final ItemStack itemStack) {
		return new NamespacedKey(
				this,
				Optional.of(itemStack)
						.map(ItemStack::getItemMeta)
						.map(ItemMeta::getDisplayName)
						.map(String::toLowerCase)
						.map(name -> name.replaceAll("\\u00A7[\\da-f]", ""))
						.map(name -> name.replaceAll("[^a-z\\d/._-]", ""))
						.orElse(getDescription().getFullName())
		);
	}

	private void setupCommand() {
		final AnnotatedCommand<MagicTntPlugin> command = CommandManager.registerCommand(MagicTntCommand.class,
				this);
		command.setOnMainCommandExecutionListener(commandSender ->
				commandSender.sendMessage(getConfigMessage("usage-error")));
		command.setOnInsufficientPermissionsListener(commandSender ->
				commandSender.sendMessage(getConfigMessage("access-denied")));
		command.setOnUnknownSubCommandExecutionListener(commandSender ->
				commandSender.sendMessage(getConfigMessage("wrong-argument")));
		command.addTypeMapper(Player.class, s -> getServer().getPlayer(s), FallbackConstants.ON_NULL);
		command.addTypeCompleter(Player.class, (commandSender, strings) ->
				getServer().getOnlinePlayers()
						.stream()
						.map(Player::getDisplayName)
						.filter(name -> name.startsWith(new ArrayList<>(strings).get(strings.size() - 1)))
						.toList());
		command.addTypeMapper(MagicTnt.class, s ->
				magicTnts.stream()
						.filter(magicTnt -> magicTnt.name().equals(s))
						.findAny()
						.orElse(null), FallbackConstants.ON_NULL);
		command.addTypeCompleter(MagicTnt.class, (commandSender, strings) ->
				magicTnts.stream()
						.map(MagicTnt::name)
						.filter(name -> name.startsWith(new ArrayList<>(strings).get(strings.size() - 1)))
						.toList());
	}
}
