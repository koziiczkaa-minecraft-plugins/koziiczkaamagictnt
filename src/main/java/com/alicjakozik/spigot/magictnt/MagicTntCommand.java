package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.annotation.TypeFallback;
import eu.andret.arguments.api.entity.ExecutorType;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.stream.Stream;

@BaseCommand("magictnt")
public class MagicTntCommand extends AnnotatedCommandExecutor<MagicTntPlugin> {
	private static final String PLAYER = "${PLAYER}";
	private static final String GIVE_MESSAGE = "give";
	private static final String GIVE_OTHER_MESSAGE = "give-other";

	public MagicTntCommand(@NotNull final CommandSender sender, @NotNull final MagicTntPlugin plugin) {
		super(sender, plugin);
	}

	@NotNull
	@TypeFallback(Player.class)
	public String playerFallback(final String player) {
		return plugin.getConfigMessage("invalid-player").replace(PLAYER, player);
	}

	@NotNull
	@TypeFallback(MagicTnt.class)
	public String magicTntFallback(final String magicTnt) {
		return plugin.getConfigMessage("invalid-tnt").replace("${TNT}", magicTnt);
	}

	@Argument(permission = "magictnt.give", executorType = ExecutorType.PLAYER)
	public String give(@NotNull final MagicTnt magicTnt) {
		return give(magicTnt, (Player) sender);
	}

	@Argument(permission = "magictnt.give", executorType = ExecutorType.PLAYER)
	public String give(@NotNull final MagicTnt magicTnt, final int amount) {
		return give(magicTnt, (Player) sender, amount);
	}

	@Argument(permission = "magictnt.give.other")
	public String give(@NotNull final MagicTnt magicTnt, @NotNull final Player player) {
		return give(magicTnt, player, 1);
	}

	@Argument(permission = "magictnt.give.other")
	public String give(@NotNull final MagicTnt magicTnt, @NotNull final Player player, final int amount) {
		final ItemStack[] itemStacks = Stream.generate(() -> magicTnt)
				.limit(amount)
				.map(MagicTnt::itemStack)
				.toArray(ItemStack[]::new);
		final Location location = player.getLocation();
		player.getInventory()
				.addItem(itemStacks)
				.values()
				.forEach(itemStack -> Optional.of(location)
						.map(Location::getWorld)
						.ifPresent(world -> world.dropItem(location, itemStack)));
		if (player == sender) {
			return plugin.getConfigMessage(GIVE_MESSAGE);
		}
		return plugin.getConfigMessage(GIVE_OTHER_MESSAGE).replace(PLAYER, player.getDisplayName());
	}
}
