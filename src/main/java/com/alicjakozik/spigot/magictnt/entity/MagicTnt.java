package com.alicjakozik.spigot.magictnt.entity;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.jetbrains.annotations.NotNull;

public record MagicTnt(@NotNull String name, int ticks, @NotNull ItemStack itemStack, @NotNull Recipe recipe,
					   int amount) {
}
