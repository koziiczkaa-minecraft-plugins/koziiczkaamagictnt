package com.alicjakozik.spigot.magictnt;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.Recipe;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class MagicTntCraftingListener implements Listener {
	@NotNull
	private final MagicTntPlugin plugin;

	public MagicTntCraftingListener(@NotNull final MagicTntPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPrepareItemCraft(@NotNull final PrepareItemCraftEvent event) {
		final Recipe eventRecipe = event.getRecipe();
		if (eventRecipe == null) {
			return;
		}
		plugin.getMagicTnts()
				.stream()
				.filter(magicTnt -> magicTnt.recipe().getResult().equals(eventRecipe.getResult()))
				.findAny()
				.ifPresent(magicTnt -> {
					final boolean isValid = Arrays.stream(event.getInventory().getMatrix())
							.allMatch(itemStack -> itemStack.getAmount() >= magicTnt.amount());
					if (!isValid) {
						event.getInventory().setResult(null);
					}
				});
	}

	@EventHandler
	public void onCraftItem(@NotNull final CraftItemEvent event) {
		final Recipe eventRecipe = event.getRecipe();
		plugin.getMagicTnts()
				.stream()
				.filter(magicTnt -> eventRecipe.getResult().equals(magicTnt.recipe().getResult()))
				.findAny()
				.ifPresent(magicTnt -> Arrays.stream(event.getInventory().getMatrix())
						.forEach(field -> field.setAmount(field.getAmount() - magicTnt.amount() + 1)));
	}
}
