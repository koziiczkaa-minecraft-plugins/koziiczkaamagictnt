package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.function.Consumer;

public class MagicTntPlayerListener implements Listener {
	@NotNull
	private static final Map<BlockFace, Consumer<Location>> BLOCK_FACE_MAP = Map.ofEntries(
			new SimpleEntry<>(BlockFace.DOWN, location -> location.add(0, 1, 0)),
			new SimpleEntry<>(BlockFace.UP, location -> location.add(0, -1, 0)),
			new SimpleEntry<>(BlockFace.WEST, location -> location.add(1, 0, 0)),
			new SimpleEntry<>(BlockFace.EAST, location -> location.add(-1, 0, 0)),
			new SimpleEntry<>(BlockFace.NORTH, location -> location.add(0, 0, 1)),
			new SimpleEntry<>(BlockFace.SOUTH, location -> location.add(0, 0, -1))
	);
	@NotNull
	private final MagicTntPlugin plugin;

	public MagicTntPlayerListener(@NotNull final MagicTntPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onBlockPlace(@NotNull final BlockPlaceEvent event) {
		final MagicTnt magicTnt = plugin.getMagicTnt(event.getItemInHand());
		if (magicTnt == null) {
			return;
		}
		final Block blockAgainst = event.getBlockAgainst();
		final Location location = blockAgainst.getLocation();
		final World world = location.getWorld();
		if (world == null) {
			return;
		}
		final BlockFace face = event.getBlockPlaced().getFace(blockAgainst);
		BLOCK_FACE_MAP.get(face).accept(location);
		final TNTPrimed tntPrimed = world.spawn(location, TNTPrimed.class);
		tntPrimed.setFuseTicks(magicTnt.ticks());
		event.setCancelled(true);
	}

	@EventHandler
	public void onPlayerInteract(@NotNull final PlayerInteractEvent event) {
		final Action action = event.getAction();
		if (action != Action.RIGHT_CLICK_AIR) {
			return;
		}
		final ItemStack item = event.getItem();
		final MagicTnt magicTnt = plugin.getMagicTnt(item);
		if (magicTnt == null) {
			return;
		}
		final Player player = event.getPlayer();
		final Location location = player.getLocation();
		final World world = location.getWorld();
		if (world == null) {
			return;
		}
		final TNTPrimed tntPrimed = world.spawn(location.add(0, 1, 0), TNTPrimed.class);
		tntPrimed.setVelocity(location.getDirection().multiply(1));
		tntPrimed.setFuseTicks(magicTnt.ticks());
		final GameMode gameMode = player.getGameMode();
		if (gameMode.equals(GameMode.CREATIVE)) {
			return;
		}
		final int amount = item.getAmount();
		item.setAmount(amount - 1);
	}
}
