package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.Recipe;
import org.mockito.Mock;
import org.mockito.testng.MockitoTestNGListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNGListener.class)
public class MagicTntCommandTest {
	@Mock
	private MagicTntPlugin plugin;
	@Mock
	private Player player;
	@Mock
	private PlayerInventory inventory;
	@Mock
	private ItemStack item;
	@Mock
	private Recipe recipe;
	private MagicTnt magicTnt;

	@BeforeMethod
	public void beforeMethod() {
		magicTnt = new MagicTnt("magic-tnt", 100, item, recipe, 1);
		lenient().when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		lenient().when(player.getInventory()).thenReturn(inventory);
	}

	@Test
	public void testPlayerFallback() {
		// given
		final MagicTntCommand command = new MagicTntCommand(player, plugin);
		when(plugin.getConfigMessage("invalid-player")).thenReturn("${PLAYER} is not online");

		// when
		final String result = command.playerFallback("test");

		// then
		assertThat(result).isEqualTo("test is not online");
	}

	@Test
	public void testMagicTntFallback() {
		// given
		final MagicTntCommand command = new MagicTntCommand(player, plugin);
		when(plugin.getConfigMessage("invalid-tnt")).thenReturn("Error: ${TNT} is invalid");

		// when
		final String result = command.magicTntFallback("test-magicTnt");

		// then
		assertThat(result).isEqualTo("Error: test-magicTnt is invalid");
	}

	@Test
	public void testGive() {
		// given
		final MagicTntCommand command = new MagicTntCommand(player, plugin);
		when(plugin.getConfigMessage("give")).thenReturn("Added magic TNT to inventory");

		// when
		final String result = command.give(magicTnt);

		// then
		verify(inventory, times(1)).addItem(item);
		assertThat(result).isEqualTo("Added magic TNT to inventory");
	}

	@Test
	public void testGiveWithAmount() {
		// given
		final MagicTntCommand command = new MagicTntCommand(player, plugin);
		when(plugin.getConfigMessage("give")).thenReturn("Added magic TNT to inventory");

		// when
		final String result = command.give(magicTnt, 2);

		// then
		verify(inventory, times(1)).addItem(item, item);
		assertThat(result).isEqualTo("Added magic TNT to inventory");
	}

	@Test
	public void testGiveWithPlayerSelf() {
		// given
		final MagicTntCommand command = new MagicTntCommand(player, plugin);
		when(plugin.getConfigMessage("give")).thenReturn("Added magic TNT to inventory");

		// when
		final String result = command.give(magicTnt, player);

		// then
		verify(inventory, times(1)).addItem(item);
		assertThat(result).isEqualTo("Added magic TNT to inventory");
	}

	@Test
	public void testGiveWithPlayerOther() {
		// given
		final CommandSender sender = mock(CommandSender.class);
		final MagicTntCommand command = new MagicTntCommand(sender, plugin);
		when(plugin.getConfigMessage("give-other"))
				.thenReturn("Added magic TNT to ${PLAYER}'s inventory");
		when(player.getDisplayName()).thenReturn("test");

		// when
		final String result = command.give(magicTnt, player);

		// then
		verify(inventory, times(1)).addItem(item);
		assertThat(result).isEqualTo("Added magic TNT to test's inventory");
	}

	@Test
	public void testGiveWithPlayerWithAmountOther() {
		// given
		final CommandSender sender = mock(CommandSender.class);
		final MagicTntCommand command = new MagicTntCommand(sender, plugin);
		when(plugin.getConfigMessage("give-other"))
				.thenReturn("Added magic TNT to ${PLAYER}'s inventory");
		when(player.getDisplayName()).thenReturn("test");

		// when
		final String result = command.give(magicTnt, player, 2);

		// then
		verify(inventory, times(1)).addItem(item, item);
		assertThat(result).isEqualTo("Added magic TNT to test's inventory");
	}

	@Test
	public void testGiveWithExceededInventorySlots() {
		// given
		final CommandSender sender = mock(CommandSender.class);
		final MagicTntCommand command = new MagicTntCommand(sender, plugin);
		final HashMap<Integer, ItemStack> itemStackHashMap =
				new HashMap<>(Map.ofEntries(new AbstractMap.SimpleEntry<>(1, item)));
		final World world = mock(World.class);
		final Location location = new Location(world, 1, 2, 3);
		when(plugin.getConfigMessage("give-other"))
				.thenReturn("Added magic TNT to ${PLAYER}'s inventory");
		when(player.getDisplayName()).thenReturn("test");
		when(inventory.addItem(item, item)).thenReturn(itemStackHashMap);
		when(player.getLocation()).thenReturn(location);

		// when
		final String result = command.give(magicTnt, player, 2);

		// then
		verify(world, times(1)).dropItem(location, item);
		assertThat(result).isEqualTo("Added magic TNT to test's inventory");
	}
}
