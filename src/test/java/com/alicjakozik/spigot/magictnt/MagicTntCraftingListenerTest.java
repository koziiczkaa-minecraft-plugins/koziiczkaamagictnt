package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import org.bukkit.Material;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.mockito.Mock;
import org.mockito.testng.MockitoTestNGListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNGListener.class)
public class MagicTntCraftingListenerTest {
	@Mock
	private MagicTntPlugin plugin;
	@Mock
	private PrepareItemCraftEvent prepareItemCraftEvent;
	@Mock
	private CraftItemEvent craftItemEvent;
	@Mock
	private Recipe recipe;
	@Mock
	private ItemStack itemStack;
	@Mock
	private CraftingInventory inventory;
	private MagicTnt magicTnt;

	@BeforeMethod
	public void beforeMethod() {
		magicTnt = new MagicTnt("magic-tnt", 100, itemStack, recipe, 2);
	}

	@Test
	public void testPrepareItemCraft() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		final ItemStack[] matrix = {
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2)
		};
		when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		when(prepareItemCraftEvent.getRecipe()).thenReturn(recipe);
		when(recipe.getResult()).thenReturn(itemStack);
		when(prepareItemCraftEvent.getInventory()).thenReturn(inventory);
		when(inventory.getMatrix()).thenReturn(matrix);

		// when
		listener.onPrepareItemCraft(prepareItemCraftEvent);

		// then
		verify(inventory, never()).setResult(null);
	}

	@Test
	public void testPrepareCraftItemWithoutRecipe() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		when(prepareItemCraftEvent.getRecipe()).thenReturn(null);

		// when
		listener.onPrepareItemCraft(prepareItemCraftEvent);

		// then
		verify(inventory, never()).setResult(null);
	}

	@Test
	public void testPrepareCraftItemWithDifferentRecipes() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		final Recipe tntRecipe = mock(Recipe.class);
		when(prepareItemCraftEvent.getRecipe()).thenReturn(tntRecipe);
		when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		when(tntRecipe.getResult()).thenReturn(new ItemStack(Material.STONE));
		when(recipe.getResult()).thenReturn(itemStack);

		// when
		listener.onPrepareItemCraft(prepareItemCraftEvent);

		// then
		verify(inventory, never()).setResult(null);
	}

	@Test
	public void testPrepareCraftItemWithInvalidAmount() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		final ItemStack[] matrix = {
				new ItemStack(Material.TNT, 1),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2),
				new ItemStack(Material.TNT, 2)
		};
		when(prepareItemCraftEvent.getRecipe()).thenReturn(recipe);
		when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		when(recipe.getResult()).thenReturn(itemStack);
		when(prepareItemCraftEvent.getInventory()).thenReturn(inventory);
		when(inventory.getMatrix()).thenReturn(matrix);

		// when
		listener.onPrepareItemCraft(prepareItemCraftEvent);

		// then
		verify(inventory, times(1)).setResult(null);
	}

	@Test
	public void testOnCraftItem() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		final ItemStack[] matrix = {
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3)
		};
		when(craftItemEvent.getRecipe()).thenReturn(recipe);
		when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		when(recipe.getResult()).thenReturn(itemStack);
		when(craftItemEvent.getInventory()).thenReturn(inventory);
		when(inventory.getMatrix()).thenReturn(matrix);

		// when
		listener.onCraftItem(craftItemEvent);

		// then
		assertThat(matrix).allMatch(itemStack -> itemStack.getAmount() == 2);
	}

	@Test
	public void testOnCraftItemWithDifferentRecipes() {
		// given
		final MagicTntCraftingListener listener = new MagicTntCraftingListener(plugin);
		final ItemStack[] matrix = {
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3),
				new ItemStack(Material.TNT, 3)
		};
		final Recipe tntRecipe = mock(Recipe.class);

		when(craftItemEvent.getRecipe()).thenReturn(tntRecipe);
		when(plugin.getMagicTnts()).thenReturn(Collections.singletonList(magicTnt));
		when(tntRecipe.getResult()).thenReturn(new ItemStack(Material.STONE));
		when(recipe.getResult()).thenReturn(itemStack);

		// when
		listener.onCraftItem(craftItemEvent);

		// then
		assertThat(matrix).allMatch(itemStack -> itemStack.getAmount() == 3);
	}
}
