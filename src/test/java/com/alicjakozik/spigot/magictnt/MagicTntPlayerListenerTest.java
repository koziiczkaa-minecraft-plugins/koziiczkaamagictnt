package com.alicjakozik.spigot.magictnt;

import com.alicjakozik.spigot.magictnt.entity.MagicTnt;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.util.Vector;
import org.mockito.Mock;
import org.mockito.testng.MockitoTestNGListener;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNGListener.class)
public class MagicTntPlayerListenerTest {
	@Mock
	private MagicTntPlugin plugin;
	@Mock
	private BlockPlaceEvent blockPlaceEvent;
	@Mock
	private PlayerInteractEvent playerInteractEvent;
	@Mock
	private ItemStack itemStack;
	@Mock
	private Block block;
	@Mock
	private Location location;
	@Mock
	private World world;
	@Mock
	private Player player;
	@Mock
	private TNTPrimed tntPrimed;
	@Mock
	private Recipe recipe;
	private final MagicTnt magicTnt = new MagicTnt("magic-tnt", 100, itemStack, recipe, 1);

	@DataProvider(name = "getBlockFaces")
	public Object[][] getBlockFaces() {
		return new Object[][]{
				{BlockFace.UP},
				{BlockFace.DOWN},
				{BlockFace.WEST},
				{BlockFace.EAST},
				{BlockFace.NORTH},
				{BlockFace.SOUTH}
		};
	}

	@Test
	public void testPlaceNonMagicTnt() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(blockPlaceEvent.getItemInHand()).thenReturn(itemStack);
		when(plugin.getMagicTnt(any(ItemStack.class))).thenReturn(null);

		// when
		listener.onBlockPlace(blockPlaceEvent);

		// then
		verify(blockPlaceEvent, times(0)).setCancelled(anyBoolean());
	}

	@Test
	public void testPlaceBlockWithNoWorld() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(blockPlaceEvent.getBlockAgainst()).thenReturn(block);
		when(block.getLocation()).thenReturn(location);
		when(location.getWorld()).thenReturn(null);
		when(blockPlaceEvent.getItemInHand()).thenReturn(itemStack);
		when(plugin.getMagicTnt(itemStack)).thenReturn(magicTnt);

		// when
		listener.onBlockPlace(blockPlaceEvent);

		// then
		verify(blockPlaceEvent, times(0)).setCancelled(anyBoolean());
	}

	@Test(dataProvider = "getBlockFaces")
	public void testPlaceMagicTnt(final BlockFace blockFace) {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(blockPlaceEvent.getBlockAgainst()).thenReturn(block);
		when(block.getLocation()).thenReturn(location);
		when(location.getWorld()).thenReturn(world);
		when(blockPlaceEvent.getItemInHand()).thenReturn(itemStack);
		when(plugin.getMagicTnt(any(ItemStack.class))).thenReturn(magicTnt);
		when(blockPlaceEvent.getBlockPlaced()).thenReturn(block);
		when(block.getFace(any(Block.class))).thenReturn(blockFace);
		when(world.spawn(any(Location.class), any())).thenReturn(tntPrimed);

		// when
		listener.onBlockPlace(blockPlaceEvent);

		// then
		verify(world, times(1)).spawn(any(Location.class), any());
		verify(tntPrimed, times(1)).setFuseTicks(100);
		verify(blockPlaceEvent, times(1)).setCancelled(true);
	}

	@Test
	public void testPlayerInteractWithWrongAction() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(playerInteractEvent.getAction()).thenReturn(Action.LEFT_CLICK_AIR);

		// when
		listener.onPlayerInteract(playerInteractEvent);

		// then
		verifyNoMoreInteractions(playerInteractEvent);
	}

	@Test
	public void testPlayerInteractNonMagicTnt() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(playerInteractEvent.getAction()).thenReturn(Action.RIGHT_CLICK_AIR);
		when(plugin.getMagicTnt(itemStack)).thenReturn(null);
		when(playerInteractEvent.getItem()).thenReturn(itemStack);

		// when
		listener.onPlayerInteract(playerInteractEvent);

		// then
		verifyNoMoreInteractions(playerInteractEvent);
	}

	@Test
	public void testPlayerInteractNoWorld() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		when(playerInteractEvent.getAction()).thenReturn(Action.RIGHT_CLICK_AIR);
		when(plugin.getMagicTnt(itemStack)).thenReturn(magicTnt);
		when(playerInteractEvent.getItem()).thenReturn(itemStack);
		when(playerInteractEvent.getPlayer()).thenReturn(player);
		when(player.getLocation()).thenReturn(location);
		when(location.getWorld()).thenReturn(null);

		// when
		listener.onPlayerInteract(playerInteractEvent);

		// then
		verifyNoMoreInteractions(playerInteractEvent);
	}

	@Test
	public void testPlayerInteractGameModeCreative() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		final Vector direction = new Vector(120, 60, 120);
		final GameMode gameMode = GameMode.CREATIVE;
		when(playerInteractEvent.getAction()).thenReturn(Action.RIGHT_CLICK_AIR);
		when(plugin.getMagicTnt(itemStack)).thenReturn(magicTnt);
		when(playerInteractEvent.getItem()).thenReturn(itemStack);
		when(playerInteractEvent.getPlayer()).thenReturn(player);
		when(player.getLocation()).thenReturn(location);
		when(location.getWorld()).thenReturn(world);
		when(world.spawn(any(Location.class), any())).thenReturn(tntPrimed);
		when(location.getDirection()).thenReturn(direction);
		when(location.add(anyDouble(), anyDouble(), anyDouble())).thenReturn(location);
		when(player.getGameMode()).thenReturn(gameMode);

		// when
		listener.onPlayerInteract(playerInteractEvent);

		// then
		verify(world, times(1)).spawn(any(Location.class), any());
		verify(tntPrimed, times(1)).setVelocity(direction);
		verify(tntPrimed, times(1)).setFuseTicks(100);
	}

	@Test
	public void testPlayerInteractMagicTnt() {
		// given
		final MagicTntPlayerListener listener = new MagicTntPlayerListener(plugin);
		final Vector direction = new Vector(120, 60, 120);
		final GameMode gameMode = GameMode.SURVIVAL;
		when(playerInteractEvent.getAction()).thenReturn(Action.RIGHT_CLICK_AIR);
		when(plugin.getMagicTnt(itemStack)).thenReturn(magicTnt);
		when(playerInteractEvent.getItem()).thenReturn(itemStack);
		when(playerInteractEvent.getPlayer()).thenReturn(player);
		when(player.getLocation()).thenReturn(location);
		when(location.getWorld()).thenReturn(world);
		when(world.spawn(any(Location.class), any())).thenReturn(tntPrimed);
		when(location.getDirection()).thenReturn(direction);
		when(itemStack.getAmount()).thenReturn(2);
		when(location.add(anyDouble(), anyDouble(), anyDouble())).thenReturn(location);
		when(player.getGameMode()).thenReturn(gameMode);

		// when
		listener.onPlayerInteract(playerInteractEvent);

		// then
		verify(world, times(1)).spawn(any(Location.class), any());
		verify(tntPrimed, times(1)).setVelocity(direction);
		verify(tntPrimed, times(1)).setFuseTicks(100);
		verify(itemStack, times(1)).setAmount(1);
	}
}
