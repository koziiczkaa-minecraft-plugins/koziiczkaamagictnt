# koziiczkaaMagicTnt

A Minecraft plugin which adds throwable TNT to the game.

## Config

In the config you can define the crafting recipe of the Magic TNT, including the item type and the amount of the item
required in one crafting field. You can also customize the Magic TNT lore and all of the messages that are being sent
both to the player, and the console.

An example config is present as the default config.

## Permissions

- `magictnt.give` - allows the player to give themselves a specific amount of the Magic TNT
- `magictnt.give.other` - allows the player to give the Magic TNT to the inventory of another player
- `magictnt.*` - grants all other permissions

## Commands

The main command is `/magictnt` (alias `/mt`).

- `/magictnt give [player] [amount]` - gives the `amount` of the Magic TNT to the `player`'s inventory (default values
  are command executor and 1)
